import React, { Component } from "react";
import './Menubar.css';
import image from './brand.jpg';





class Menubar extends Component{
    render(){
        return<div class="total_body"> 
        
                              {/* Side Navbar */}
        
        <div class="wrapper d-flex align-items-stretch">
            <nav id="sidebar">
                <div class="p-4 pt-5">
                    
                    <a href="#"><img src={image} height={130} width={120} class="img brand rounded-circle mb-5" alt=""/></a>
                        
                        <div className="Profile">
                            <a class="nav-link" href="#"><i class="fa fa-university icon3" aria-hidden="true"></i>School Profile</a>
                        </div>

                    <ul class="list-unstyled components mb-5">
                        <li>
                            <a href="#"><i class="fa fa-users icons2" aria-hidden="true"></i>&nbsp;&nbsp;Profile Update</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa-solid fa-desktop icons2"></i>&nbsp;&nbsp;Online Test</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa-sharp fa-solid fa-indian-rupee-sign icons2"></i>&nbsp;&nbsp;Fee Details</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa-solid fa-sack-dollar icons2"></i>&nbsp;&nbsp;Staff Salaries</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa-solid fa-person-chalkboard icons2"></i>&nbsp;&nbsp;Staff Details</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa-solid fa-person icons2"></i>&nbsp;&nbsp;Student Details</a>
                        </li>
                        <li>
                            <div class="dropdown">   
                                <p class="dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                <i class="fa-solid fa-book icons2"></i>&nbsp;&nbsp;Attendance
                                </p>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                    <li class="ant-menu-item" role="menuitem"><a class="dropdown-item" href="#">Staff</a></li>
                                    <li class="ant-menu-item" role="menuitem"><a class="dropdown-item" href="#">Student</a></li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <div class="dropdown">   
                                <p class="dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                <i class="fa-solid fa-address-card icons2"></i>&nbsp;&nbsp;Progress Report
                                </p>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                    <li class="ant-menu-item" role="menuitem"><a class="dropdown-item" href="#">Teacher's Login</a></li>
                                    <li class="ant-menu-item" role="menuitem"><a class="dropdown-item" href="#">Student's Login</a></li>
                                    <li class="ant-menu-item" role="menuitem"><a class="dropdown-item" href="#">Parent's Login</a></li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <a href="#"><i class="fa-solid fa-calendar-days icons2"></i>&nbsp;&nbsp;Events</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa-solid fa-bus icons2"></i>&nbsp;&nbsp;Vehicles</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa-solid fa-route icons2"></i>&nbsp;&nbsp;Routes</a>
                        </li> 
                    </ul>
                </div>
            </nav>


                                {/* <!-- Header  --> */}

            <div id="content" class="p-4 p-md-5">

              <nav class="navbar navbar-expand-lg navbar-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="btn btn-primary">
                       <i class="fa fa-bars" aria-hidden="true"></i>
                    </button>

                    {/* <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fa fa-bars"></i>
                    </button> */}

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="nav navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="#"><i class="fa-solid fa-book icons1"></i>&nbsp;Library</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#"><i class="fa fa-certificate icons1" aria-hidden="true"></i>&nbsp;Careers</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#"><i class="fa fa-address-book icons1" aria-hidden="true"></i>&nbsp;Contact</a>
                            </li>
                            <li>
                                <div class="container h-100">
                                    <div class="d-flex justify-content-center h-100">
                                        <div class="search">
                                        <input class="search_input" type="text" name="" placeholder="Search here..."/>
                                        <a href="#" class="search_icon"><i class="fa fa-search icobtn"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </li>                           
                        </ul>   
                    </div>
                </div>
              </nav>   
            </div>
        </div>

                      {/* Content Here */}

             <p id="content1">Content Here</p>    
      
        
    </div>

    }
}




export default Menubar;